from lcm_video_export import frame_2_msg, FrameTimer
import sys
import lcm
import time
import numpy as np
from mwt.image_t import image_t
from mwt.bounding_box_t import bounding_box_t
import cv2

box_done = False
box_time = 0

def send_video(video_path, fps=10, chan="VIDEO"):

    # setup lcm and image_t
    lc = lcm.LCM()
    img_msg = image_t()

    # open video
    print("Opening: " + video_path)
    cap = cv2.VideoCapture(video_path)

    # Check if video opened successfully
    if (cap.isOpened() == False):
        print("Error opening video stream or file")

    timer = FrameTimer(fps=fps)

    # Read until video is completed and report frame rate periodically
    try:
        while cap.isOpened():

            # time the read and send so we know how long to sleep
            timer.fps_start()

            # read and send the frame
            ret, frame = cap.read()
            frame = cv2.resize(frame, (1032, 580))
            if ret:
                frame_2_msg(img_msg, frame, pixel_type='mono')
                img_msg.utime = int(time.time()*1000000)
                timer.new_frame()
                lc.publish(chan, img_msg.encode())
            else:
                break

            # sleep to match fps and possibly log frame rate
            timer.fps_sleep()
            timer.log()


    except KeyboardInterrupt:
        pass

def scale_points(p0, p1, scale0, scale1):
    scale = [scale0, scale1]
    p0_out = [0, 0]
    p1_out = [0, 0]
    for i in range(0, len(p0)):
        p0_out[i] = int(p0[i] * scale[i])
        p1_out[i] = int(p1[i] * scale[i])

    return tuple(p0_out), tuple(p1_out)

def process_boxes(image_buffer, score_threshold, boxes, scale_x, scale_y):

    output_boxes = []

    for box in boxes:
        pt0, pt1, utime, class_name, scores = box
        if np.max(scores) > score_threshold:
            pt0, pt1 = scale_points(pt0, pt1, scale_x, scale_y)
            cv2.rectangle(image_buffer, tuple(pt0), tuple(pt1), (3, 177, 252), 2)
            cv2.putText(image_buffer, class_name, (pt0[0], pt1[1] + 20), cv2.FONT_HERSHEY_COMPLEX, 0.75, (3, 252, 53))

    return image_buffer

def send_to_ml(video_path, chan='VIDEO', box_channel="BOUNDING_BOX"):

    box_buffer = []
    global box_done
    box_done = False
    global box_time
    box_time = int(time.time()*1000000)

    def handle_box_message(channel, data):
        box = bounding_box_t.decode(data)
        global box_time
        print([box.utime, box_time])
        if box.utime == box_time:
            pt0 = (round(box.left), round(box.top))
            pt1 = (round(box.left + box.width), round(box.top + box.height))
            box_buffer.append((pt0, pt1, box.utime, box.class_name, box.scores))
        else:
            print('Box Done')
            global box_done
            box_done = True

    # setup lcm and image_t
    lc = lcm.LCM()
    img_msg = image_t()

    # open video
    print("Opening: " + video_path)
    cap = cv2.VideoCapture(video_path)
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    output = cv2.VideoWriter(video_path[:-4] + '_ML' + '.avi', fourcc, 60, (1920,1080))

    # Check if video opened successfully
    if (cap.isOpened() == False):
        print("Error opening video stream or file")

    scale_x = 1920.0/1032.0
    scale_y = scale_x

    subscription = lc.subscribe(box_channel, handle_box_message)

    # Read until video is completed and report frame rate periodically
    try:
        while cap.isOpened():

            # read and send the frame
            ret, frame = cap.read()
            org_frame = frame.copy()
            frame = cv2.resize(frame, (1032, 580))
            if ret:
                frame_2_msg(img_msg, frame, pixel_type='mono')
                img_msg.utime = int(time.time()*1000000)
                lc.publish(chan + '_LEFT', img_msg.encode())
                lc.publish(chan + '_RIGHT', img_msg.encode())
            else:
                break

            # Wait for ML output
            box_time = img_msg.utime
            while True:
                if lc.handle_timeout(200) == 0:
                    break

            org_frame = process_boxes(org_frame, 0.14, box_buffer, scale_x, scale_y)
            output.write(org_frame)

            org_frame = cv2.resize(org_frame, (960, 540))
            cv2.imshow("ML output", org_frame)

            cv2.waitKey(10)

            box_buffer = []
            box_done = False


    except KeyboardInterrupt:
        pass

    output.release()

if __name__=="__main__":

    input_video = sys.argv[1]
    send_to_ml(input_video, chan=sys.argv[2])

