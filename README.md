# lcm-video-export


## Setup (linux/mac)

#### Install miniconda and activate environment

- Follow instructions here: https://docs.conda.io/en/latest/miniconda.html

#### Install additional python modules

```bash
pip install opencv-python-headless
pip install imageio
pip install imageio-ffmpeg
```

#### Build and install lcm (Linux)

- Follow instructions here to build lcm: https://lcm-proj.github.io/build_instructions.html

##### Build python lcm module
```bash 
cd lcm/lcm-python
python setup.py install
```

#### Build and install lcm (Windows)

A range of LCM type definitions are included along with the "LogReader" to facilitate easy export of video files and lcm data without a proper LCM installation (tested on Windows).
New LCM types are continually developed and may not be included in this repository; perform a full LCM install if you encounter issues. Follow the instructions here: https://docs.google.com/document/d/10JZ-NxreJbZ2kfL9cn8dFh4JTxNWvsG3kS2ko4sFtNs/edit?usp=sharing

## Usage

#### Run on the command line
```bash
python lcm_video_export.py [lcmlogdir] [deployment_id] [lcmchannel1] [lcmchannel2] [outputdir] [display]
```
for example, to export videos from Oct 29, 2020:
```bash
python lcm_video_export.py lcmLogs.Oct.29.2020 163 MANTA_LEFT MANTA_RIGHT ~/
```
Or to export a list of state changes, use lcm_state_export:
```bash
python lcm_state_export.py [lcm-log-dir] [deployment_id] [output-dir] [timestamp YYMMDDTHHMMSS]")
```
To export a list of target locations in each camera, use lcm_target_export
```bash
python lcm_target_export.py [lcm-log-dir] [deployment_id] [output-dir] [timestamp YYMMDDTHHMMSS]")
python lcm_target_export.py "lcmLogs.July.19.2021" "1365" "lcm_targetdata"
```