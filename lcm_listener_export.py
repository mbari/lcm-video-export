import os
from mwt.image_t import image_t
import lcm
import datetime
import cv2
import imageio
import numpy as np


"""
msg_2_frame - decode an lcm message into a frame
"""
def msg_2_frame(msg):

    frame = np.frombuffer(msg.data, dtype=np.uint8)
    # a few conditions here for image types, many more are possible
    bytes_per_pixel = 1 # assume mono images
    # handle color images
    if msg.pixelformat == image_t.PIXEL_FORMAT_RGB:
        bytes_per_pixel = 3

    # extract the frame
    frame = np.reshape(frame, newshape=(msg.height, msg.width, bytes_per_pixel))

    # tile monoimage to rgb if needed
    if bytes_per_pixel == 1:
        frame = np.tile(frame, (1, 1, 3))

    return frame

class ListenerExport:

    def __init__(self, left='MANTA_LEFT', right='MANTA_RIGHT', path='.', fps=10):

        self.left_channel = left
        self.right_channel = right
        self.output_path = path
        self.fps = fps

        if not os.path.exists(self.output_path):
            os.makedirs(self.output_path)

        self.left_video_path = ''
        self.right_video_path = ''
        self.left_video_output = None
        self.right_video_output = None
        self.left_frame = None
        self.right_frame = None
        self.record = False

    def handle_frame(self, channel, data):

        msg = image_t.decode(data)

        # Print out the timestamp in human readable format
        if len(str(msg.utime)) < 16:
            msg.utime = msg.utime * 1000

        print(datetime.datetime.utcfromtimestamp(msg.utime/1000000.0).strftime('%Y-%m-%d %H:%M:%S'))

        frame = msg_2_frame(msg)

        if channel == self.left_channel:

            # resize to fit video format
            frame = cv2.resize(frame, (1040, 784))
            self.left_frame = frame

            if not self.record:
                return

            if self.left_video_output is None:
                self.left_video_path = os.path.join(self.output_path, self.left_channel + '_' + str(msg.utime) + '.mp4')
                print('Creating video: ' + self.left_video_path)
                self.left_video_output = imageio.get_writer(self.left_video_path,
                                                         format='FFMPEG',
                                                         mode='I',
                                                         fps=self.fps,
                                                         quality=10)

            self.left_video_output.append_data(frame)

        elif channel == self.right_channel:

            frame = cv2.resize(frame, (1040, 784))
            self.right_frame = frame

            if not self.record:
                return

            if self.right_video_output is None:
                self.right_video_path = os.path.join(self.output_path, self.right_channel + '_' + str(msg.utime) + '.mp4')
                print('Creating video: ' + self.right_video_path)
                self.right_video_output = imageio.get_writer(self.right_video_path,
                                                         format='FFMPEG',
                                                         mode='I',
                                                         fps=self.fps,
                                                         quality=10)
            # resize to fit video format

            self.right_video_output.append_data(frame)


if __name__ == '__main__':

    lc = lcm.LCM()
    exporter = ListenerExport(path='/home/ops/')

    lc.subscribe(exporter.left_channel, exporter.handle_frame)
    lc.subscribe(exporter.right_channel, exporter.handle_frame)

    try:
        while True:

            # Check for exit from user
            if exporter.left_frame is not None:
                cv2.imshow(exporter.left_channel, exporter.left_frame)
            k = cv2.waitKey(10) & 0xFF
            if k == ord('q'):
                break
            if k == ord('r'):
                exporter.record = not exporter.record
                if exporter.record:
                    print('Recording enabled')
                else:
                    print('Recording disabled')


            lc.handle_timeout(1000)
    except KeyboardInterrupt:
        pass

    if exporter.left_video_output is not None:
        print('Closing video: ' + exporter.left_video_path)
        exporter.left_video_output.close()
    if exporter.right_video_output is not None:
        print('Closing video: ' + exporter.right_video_path)
        exporter.right_video_output.close()

