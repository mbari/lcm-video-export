# -*- coding: utf-8 -*-
"""
lcm_target_export.py -- Export file with target data from LCM
Based on lcm_video_export.py
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.
"""

import os
import glob
import time
import sys
from mwt.image_t import image_t
from mwt.mwt_target_t import mwt_target_t
from mwt.mini_rov_depth_t import mini_rov_depth_t
from mwt.mwt_control_status_t import mwt_control_status_t
from mwt.supervisor_cfg_t import supervisor_cfg_t
from mwt.mini_rov_status_t import mini_rov_status_t
from mwt.ext_target_t import ext_target_t
from lcm_reader import LogExtractor
from lcm_reader import LogReader
# import lcm
import cv2
import imageio
import numpy as np
from datetime import datetime, timezone
import csv

LCM_CHANNEL = 'Vim0'
PIXEL_TYPE = 'mono'

"""
FrameTimer - time frame events and estimate and log frame rate.
"""


class FrameTimer:

    def __init__(self, fps=60, log_rate=1, fps_samples=10):
        self.frames = 0
        self.fps_time = 0
        self.log_time = 0
        self.log_rate = log_rate
        self.fps = fps
        self.fps_samples = np.zeros((10, 1))
        self.fps_samples_index = 0

    def new_frame(self):
        self.frames += 1
        self.fps_samples = np.roll(self.fps_samples, -1, axis=0)
        self.fps_samples[-1] = time.time()

    def estimate_fps(self):
        fps_est = self.fps_samples[1:-1] - self.fps_samples[0:-2]
        fps_est = np.mean(fps_est)
        if fps_est > 0:
            return 1 / fps_est
        else:
            return 0.0

    def fps_start(self):
        self.fps_time = time.time()

    def log_start(self):
        self.log_time = time.time()

    def fps_sleep(self):
        try:
            time.sleep(1 / self.fps - (time.time() - self.fps_time))
        except:
            pass

    def log(self):
        if (time.time() - self.log_time) > 1 / self.log_rate:
            print('Frames: ' +
                  str(self.frames) + ', FPS: ' + str(self.estimate_fps())
                  )
            self.log_time = time.time()


"""
frame_2_msg - convert a frame from the video (camera) into an lcm message
"""


def frame_2_msg(msg, frame, pixel_type=PIXEL_TYPE):
    msg.utime = int(time.time() * 1000)
    msg.width = frame.shape[1]
    msg.height = frame.shape[0]
    if pixel_type == 'mono':
        labColor = cv2.cvtColor(frame, cv2.COLOR_RGB2LAB)
        msg.data = labColor[:, :, 0].tobytes()
        msg.pixelformat = image_t.PIXEL_FORMAT_GRAY
    else:
        msg.data = frame.tobytes()
        msg.pixelformat = image_t.PIXEL_FORMAT_RGB

    msg.size = len(msg.data)

    return msg


"""
msg_2_frame - decode an lcm message into a frame
"""


def msg_2_frame(msg):
    frame = np.frombuffer(msg.data, dtype=np.uint8)
    # a few conditions here for image types, many more are possible
    if msg.pixelformat == image_t.PIXEL_FORMAT_RGB:
        bytes_per_pixel = 3
    if msg.pixelformat == image_t.PIXEL_FORMAT_GRAY:
        bytes_per_pixel = 1
    frame = np.reshape(frame, newshape=(msg.height, msg.width, bytes_per_pixel))

    # tile monoimage to rgb if needed
    if bytes_per_pixel == 1:
        frame = np.tile(frame, (1, 1, 3))

    return frame


def get_timecode(utime):
    # handle timestamps in micro and milli
    if len(str(utime)) == 16:
        utime = float(utime) / 1000000
    elif len(str(utime)) == 13:
        utime = float(utime) / 1000
    elif len(str(utime)) != 10:
        print('Unknown timestamp type in time code calculation.')

    dt = datetime.fromtimestamp(utime, timezone.utc)

    # return dt.strftime('%Y%m%dT%H%M%SZ'), utime * 1000000
    return dt.strftime('%Y-%m-%d %H:%M:%S'), utime * 1000000


def get_file_name(prefix, timecode, postfix, ext):
    return prefix.replace('_', "") + '_' + timecode + '_' + postfix + ext


def open_video(d_id, utime, timecode, outputdir, chan, fps=10, vehicle='M'):
    # open video file
    print("Video Start Time: " + timecode)
    video_start_time = utime
    video_name = get_file_name(vehicle + d_id, timecode, chan, '.mp4')

    # create subdirs in data path from deployment ID and date
    subdir = os.path.join(outputdir, timecode[4:6], d_id)

    if not os.path.exists(subdir):
        os.makedirs(subdir)

    video_path = os.path.join(subdir, video_name)

    video_format = cv2.VideoWriter_fourcc(*'H264')

    """
    #print('Creating: ' + video_path)
    out = cv2.VideoWriter(video_path,
                          video_format,
                          fps,
                          (msg.width, msg.height))
    """
    out = imageio.get_writer(video_path,
                             format='FFMPEG',
                             mode='I',
                             fps=fps,
                             output_params=['-movflags', '+faststart'],
                             quality=9.5)

    return out


def handle_event(out, event, d_id, outputdir, chan, fps, video_start_time, minutes_per_file):
    # get data from lcm
    msg = image_t.decode(event.data)
    frame = msg_2_frame(msg)
    timecode, utime = get_timecode(msg.utime)

    # create new video file if needed
    if out is None:
        out = open_video(d_id, utime, timecode, outputdir, chan, fps)
        video_start_time = utime

    # write out the frame to the video file
    # out.write(frame)
    # frame = cv2.resize(frame, (1040, 784))
    frame = np.pad(frame, ((0, 12), (0, 8), (0, 0)), mode='reflect')
    out.append_data(frame)
    print("Elapsed Time (s)   : " + "{:.2f}".format((utime - video_start_time) / 1e6), end="\r")

    if video_start_time is not None:
        if utime - video_start_time > minutes_per_file * 60 * 1000000:
            # close video file
            print("Video Close Time: " + str(get_timecode(msg.utime)[1]))
            print("Time Delta (us) : " + str(utime - video_start_time - minutes_per_file * 60 * 1000000))
            # out.release()
            out.close()
            out = None

    return out, frame, video_start_time

def handle_target_event(out, event, d_id, outputdir):
    # get data from lcm
    msg = mwt_target_t.decode(event.data)
    # print(msg)
    # frame = msg_2_frame(msg)
    timecode, utime = get_timecode(event.header.timestamp)

    # create new file if needed
    if out is None:
        fname = event.file.name + '_targetdata.csv'
        with open(fname, 'w', newline='') as out:
            out = csv.writer(fname, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            out.writerow(['time', 'range_meters', 'bearing_degrees', 'z_meters', 'range_valid', 'bearing_valid',
                          'z_valid', 'left_pix_x', 'left_pix_y', 'right_pix_x', 'right_pix_y'])
        start_time = None

    # write the data to CSV
    # out.writerow([timecode, str(msg.range_meters), str(msg.bearing_degrees),
    #               str(msg.z_meters), str(msg.range_valid), str(msg.bearing_valid), str(msg.z_valid),
    #               str(msg.left_pix_x), str(msg.left_pix_y), str(msg.right_pix_x), str(msg.right_pix_y)])
    # out.write(frame)
    # frame = cv2.resize(frame, (1040, 784))
    # frame = np.pad(frame, ((0, 12), (0, 8), (0, 0)), mode='reflect')
    # out.append_data(frame)
    # print("Elapsed Time (s)   : " + "{:.2f}".format((utime - start_time) / 1e6), end="\r")

    # if start_time is not None:
    #     if utime - start_time > minutes_per_file * 60 * 1000000:
    #         # close video file
    #         print("Video Close Time: " + str(get_timecode(msg.utime)[1]))
    #         print("Time Delta (us) : " + str(utime - start_time - minutes_per_file * 60 * 1000000))
    #         # out.release()
    #         out.close()
    #         out = None

    return out


def export_log_files(inputdir, d_id="dep", outputdir=None):
    out = None
    last_utime = None
    start_time1 = None
    out1 = None
    out2 = None
    # chan1 = "MWT_TARGET"
    chan1 = "UNDET"
    if outputdir is None:
        outputdir = inputdir

    fname = outputdir + '\\M' + d_id + '_lcm_targetdata.csv'
    with open(fname, "w", newline='') as myfile:
        out = csv.writer(myfile, delimiter=',',
                         quotechar='|', quoting=csv.QUOTE_MINIMAL)
        out.writerow(['time', 'utime', 'depth_meters', 'state', 'class_name', 'left_x', 'left_y',
                      'range_meters', 'classes_to_track'])
        depth = 0
        oldstate = ''
        class_name = ''
        leftx = ''
        lefty = ''
        range = ''
        adv_nav = True
        class_track = ''

        for lcm_log in sorted(glob.glob(os.path.join(inputdir, 'lcmlog-*'))):
            print(lcm_log)

            try:
                # log = lcm.EventLog(lcm_log, "r")
                # log = LogExtractor(lcm_log)
                # print("Log extracted.")
                log2 = LogReader(lcm_log)
                print("Reading" + lcm_log)
                # print(str(len(log.index)) + ' messages')

                while event := log2.next(read_data=True, data_channels=[chan1, "EXT_TARGET_3", "MWT_TARGET",
                                                                        "MINIROV_DEPTH", "SUPERVISOR_CFG", "MINIROV_STATUS"]):
                    # print(event)
                    # print(event.data)
                    # for event1 in log.index:
                    try:
                        # event = log2.next(True, chan1)
                        # print(event.data)
                        if event.channel == chan1:
                            # out1, frame, start_time1 = handle_event(out1, event, d_id, outputdir, chan1, fps,
                            #                                           start_time1, minutes_per_file)
                            # out1 = handle_target_event(out1, event, d_id, outputdir)
                            msg = mwt_target_t.decode(event.data)
                            timecode, utime = get_timecode(event.header.timestamp)
                            state = ""
                            class_name = ""
                            out.writerow([timecode, utime, str(depth), str(state), class_name, str(msg.left_pix_x),
                                          str(msg.left_pix_y), str(msg.range_meters)])
                        elif event.channel == "MINIROV_DEPTH":
                            msg = mini_rov_depth_t.decode(event.data)
                            depth = msg.depth
                            # print(depth)
                        elif event.channel == "EXT_TARGET_3":
                            msg = ext_target_t.decode(event.data)
                            timecode, utime = get_timecode(event.header.timestamp)
                            state = ""
                            class_name = str(msg.left_class_name)
                            range = ""
                            # out.writerow([timecode, str(depth), str(state), class_name, str(msg.left_x),
                            #               str(msg.left_y), range])
                        elif event.channel == "MINIROV_STATUS":
                            msg = mini_rov_status_t.decode(event.data)
                            adv_nav = msg.adv_nav
                        elif event.channel == "MWT_TARGET":
                            msg = mwt_target_t.decode(event.data)
                            timecode, utime = get_timecode(event.header.timestamp)
                            msg = mwt_target_t.decode(event.data)
                            timecode, utime = get_timecode(event.header.timestamp)
                            state = ""
                            leftx = str(msg.left_pix_x)
                            lefty = str(msg.left_pix_y)
                            range = str(msg.range_meters)
                            out.writerow([timecode, utime, str(depth), str(state), class_name, leftx,
                                          lefty, range, ''])
                        # elif event.channel == "MWT_CONTROL_STAT":
                        #     try:
                        #         msg = mwt_control_status_t.decode(event.data)
                        #         print('mwt_control success')
                        #         if msg.is_pilot_enabled:
                        #             timecode, utime = get_timecode(event.header.timestamp)
                        #             out.writerow([timecode, str(depth), 'PILOT', '', '', "", "", ""])
                        #     except:
                        #         print("mwt_control error")
                        #
                        #     # state = "pi" \
                        #     #         ""  # +str(msg.is_pilot_enabled)+",control:"+str(msg.is_control_enabled)
                        #     # out.writerow([timecode, str(depth), str(state), '','', "", "", ""])
                        elif event.channel == "SUPERVISOR_CFG":
                            try:
                                msg = supervisor_cfg_t.decode(event.data)
                                state = str(msg.state_name)
                                # print("supervisor success")
                            except:
                                print("supervisor error")
                                state = ''
                            timecode, utime = get_timecode(event.header.timestamp)
                            # class_name = ""
                            # range = ""
                            if not adv_nav:
                                state = 'pilot'
                            if state != oldstate:
                                if (state == 'idle') | (state == 'search'):
                                    class_name = ''
                                    leftx = ''
                                    lefty = ''
                                    range = ''
                                if state == 'search':
                                    class_track = msg.classes_to_track.replace(',', '&')
                                else:
                                    class_track = ''
                                out.writerow([timecode, utime, str(depth), state, class_name, leftx, lefty, range, class_track])
                            oldstate = state
                    except Exception as exc:
                        print(exc)
                        pass

            except Exception as inst:
                print(inst)
                pass

        print('Done writing to ' + fname + '.')


"""
Main entry point when called from the command line
"""
if __name__ == "__main__":

    print("lcm_video_export:")

    if len(sys.argv) < 3:
        print(
            "Usage: python lcm_state_export.py [lcm-log-dir] [deployment_id] [output-dir] [timestamp YYMMDDTHHMMSS]")
        exit()

    outdir = None
    scope = None
    if len(sys.argv) >= 4:
        outdir = sys.argv[3]

    if len(sys.argv) == 5:
        scope = sys.argv[4]
        export_log_files(sys.argv[1], sys.argv[2], outdir, scope)
    else:
        export_log_files(sys.argv[1], sys.argv[2], outdir)


