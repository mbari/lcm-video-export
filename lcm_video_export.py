# -*- coding: utf-8 -*-
"""
lcm_video_export.py -- Convert images from lcm log files to timestamped video files
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.
"""

import os
import glob
import time
import sys
from mwt.image_t import image_t
from lcm_reader import LogExtractor
from lcm_reader import LogReader
# import lcm
import cv2
import imageio
import numpy as np
from datetime import datetime, timezone

LCM_CHANNEL = 'Vim0'
PIXEL_TYPE = 'mono'

"""
FrameTimer - time frame events and estimate and log frame rate.
"""


class FrameTimer:

    def __init__(self, fps=60, log_rate=1, fps_samples=10):
        self.frames = 0
        self.fps_time = 0
        self.log_time = 0
        self.log_rate = log_rate
        self.fps = fps
        self.fps_samples = np.zeros((10, 1))
        self.fps_samples_index = 0

    def new_frame(self):
        self.frames += 1
        self.fps_samples = np.roll(self.fps_samples, -1, axis=0)
        self.fps_samples[-1] = time.time()

    def estimate_fps(self):
        fps_est = self.fps_samples[1:-1] - self.fps_samples[0:-2]
        fps_est = np.mean(fps_est)
        if fps_est > 0:
            return 1 / fps_est
        else:
            return 0.0

    def fps_start(self):
        self.fps_time = time.time()

    def log_start(self):
        self.log_time = time.time()

    def fps_sleep(self):
        try:
            time.sleep(1 / self.fps - (time.time() - self.fps_time))
        except:
            pass

    def log(self):
        if (time.time() - self.log_time) > 1 / self.log_rate:
            print('Frames: ' +
                  str(self.frames) + ', FPS: ' + str(self.estimate_fps())
                  )
            self.log_time = time.time()


"""
frame_2_msg - convert a frame from the video (camera) into an lcm message
"""


def frame_2_msg(msg, frame, pixel_type=PIXEL_TYPE):
    msg.utime = int(time.time() * 1000)
    msg.width = frame.shape[1]
    msg.height = frame.shape[0]
    if pixel_type == 'mono':
        labColor = cv2.cvtColor(frame, cv2.COLOR_RGB2LAB)
        msg.data = labColor[:, :, 0].tobytes()
        msg.pixelformat = image_t.PIXEL_FORMAT_GRAY
    else:
        msg.data = frame.tobytes()
        msg.pixelformat = image_t.PIXEL_FORMAT_RGB

    msg.size = len(msg.data)

    return msg


"""
msg_2_frame - decode an lcm message into a frame
"""


def msg_2_frame(msg):
    frame = np.frombuffer(msg.data, dtype=np.uint8)
    # a few conditions here for image types, many more are possible
    if msg.pixelformat == image_t.PIXEL_FORMAT_RGB:
        bytes_per_pixel = 3
    if msg.pixelformat == image_t.PIXEL_FORMAT_GRAY:
        bytes_per_pixel = 1
    frame = np.reshape(frame, newshape=(msg.height, msg.width, bytes_per_pixel))

    # tile monoimage to rgb if needed
    if bytes_per_pixel == 1:
        frame = np.tile(frame, (1, 1, 3))

    return frame


def get_timecode(utime):
    # handle timestamps in micro and milli
    if len(str(utime)) == 16:
        utime = float(utime) / 1000000
    elif len(str(utime)) == 13:
        utime = float(utime) / 1000
    elif len(str(utime)) != 10:
        print('Unknown timestamp type in time code calculation.')

    dt = datetime.fromtimestamp(utime, timezone.utc)

    return dt.strftime('%Y%m%dT%H%M%SZ'), utime * 1000000


def get_file_name(prefix, timecode, postfix, ext):
    return prefix.replace('_', "") + '_' + timecode + '_' + postfix + ext


def open_video(d_id, utime, timecode, outputdir, chan, fps=10, vehicle='D'):
    # open video file
    print("Video Start Time: " + timecode)
    video_start_time = utime
    video_name = get_file_name(vehicle + d_id, timecode, chan, '.mp4')

    # create subdirs in data path from deployment ID and date
    subdir = os.path.join(outputdir, timecode[4:6], d_id)

    if not os.path.exists(subdir):
        os.makedirs(subdir)

    video_path = os.path.join(subdir, video_name)

    video_format = cv2.VideoWriter_fourcc(*'H264')

    """
    #print('Creating: ' + video_path)
    out = cv2.VideoWriter(video_path,
                          video_format,
                          fps,
                          (msg.width, msg.height))
    """
    out = imageio.get_writer(video_path,
                             format='FFMPEG',
                             mode='I',
                             fps=fps,
                             output_params=['-movflags', '+faststart'],
                             quality=9.5)

    return out


def handle_event(out, event, d_id, outputdir, chan, fps, video_start_time, minutes_per_file):
    # get data from lcm
    msg = image_t.decode(event.data)
    frame = msg_2_frame(msg)
    timecode, utime = get_timecode(msg.utime)

    # create new video file if needed
    if out is None:
        out = open_video(d_id, utime, timecode, outputdir, chan, fps)
        video_start_time = utime

    # write out the frame to the video file
    # out.write(frame)
    # frame = cv2.resize(frame, (1040, 784))
    frame = np.pad(frame, ((0, 12), (0, 8), (0, 0)), mode='reflect')
    out.append_data(frame)
    print("Elapsed Time (s)   : " + "{:.2f}".format((utime - video_start_time) / 1e6), end="\r")

    if video_start_time is not None:
        if utime - video_start_time > minutes_per_file * 60 * 1000000:
            # close video file
            print("Video Close Time: " + str(get_timecode(msg.utime)[1]))
            print("Time Delta (us) : " + str(utime - video_start_time - minutes_per_file * 60 * 1000000))
            # out.release()
            out.close()
            out = None

    return out, frame, video_start_time


def export_log_files(inputdir, d_id="dep", chan1=LCM_CHANNEL, chan2="", outputdir='', fps=10, display=True):
    out = None
    last_utime = None
    video_start_time1 = None
    video_start_time2 = None
    out1 = None
    out2 = None
    minutes_per_file = 15
    if len(outputdir) == 0:
        outputdir = inputdir

    for lcm_log in sorted(glob.glob(os.path.join(inputdir, 'lcmlog-*'))):

        try:
            # log = lcm.EventLog(lcm_log, "r")
            log = LogExtractor(lcm_log)
            log2 = LogReader(lcm_log)
            print(lcm_log)
            print(len(log.index))

            for event1 in log.index:
                try:
                    event = log2.next(True)
                    if event.channel == chan1:
                        out1, frame, video_start_time1 = handle_event(out1, event, d_id, outputdir, chan1, fps,
                                                                  video_start_time1, minutes_per_file)
                        if display:
                            cv2.imshow(chan1, frame)
                            cv2.waitKey(1)
                    if event.channel == chan2:
                        out2, frame, video_start_time2 = handle_event(out2, event, d_id, outputdir, chan2, fps,
                                                                  video_start_time2, minutes_per_file)
                        if display:
                            cv2.imshow(chan2, frame)
                            cv2.waitKey(1)
                except Exception as exc:
                    print(exc)
                    pass
        except Exception as inst:
            print('Not a valid log file')
            pass


"""
Main entry point when called from the command line
"""
if __name__ == "__main__":

    print("lcm_video_export:")

    if len(sys.argv) < 6:
        print(
            "Usage: python lcm_video_export.py [lcm-log-dir] [deployment_id] [lcm-channel1] [lcm-channel2] [outputpath] [display]")
        exit()

    display = False
    if len(sys.argv) == 7:
        display = True

    export_log_files(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], display=display)


